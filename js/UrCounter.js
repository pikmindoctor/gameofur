"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrCounter {
    constructor(player, cell) {
        this.player = player;
        this.cell = cell;
        this.movable = false;
        this.scored = false;
        if (cell != null) {
            cell.counter = this;
        }
    }

    place(cell) {
        if (cell == null) {
            console.error("Tried to place counter on a null cell");
        } else {
            if (this.cell != null) {
                this.cell.counter = null;
            }
            this.cell = cell;
            this.cell.counter = this;
        }
    }

    nextSpaceNumber(nSpaces) {
        if (this.cell != null) {
            return this.cell.pathStep + nSpaces;
        } else {
            console.error("nextSpaceNumber: Bug alert! The cell at " + nSpaces + " forward is null");
            return null;
        }
    }

    spacesAhead(nSpaces) {
        return this.player.fullPath[this.nextSpaceNumber(nSpaces)];
    }

    moveForward(nSpaces) {
        this.cell.counter = null;
        this.cell = this.spacesAhead(nSpaces);
        if (this.cell == null) {
            console.error("moveForward: Bug alert! The cell at " + nSpaces + " forward is null");
        } else {
            if (this.cell.counter != null) {
                this.cell.counter.player.counterTaken(this.cell.counter);
            }
            this.cell.counter = this;
        }
    }
}
