"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrView {
    constructor(props) {
        this.model = null;
        this.lm = props.lm;
        this.canvasID = canvasID;
        this.laf = props.laf;
        this.ctx = null;
        this.uiManager = null;
        this.canvas = document.getElementById(canvasID);
        this.ctx = this.canvas.getContext("2d");
        this.ctx.webkitImageSmoothingEnabled = true;
        this.updateCounterSize();
        this.laf.updateFonts();
        this.animation = null;
        let imgSrc = null;
        let imagesToLoad = [];
        imagesToLoad.push("./img/Rosette.svg");
        imagesToLoad.push("./img/FinalFinish.svg");
        imagesToLoad.push(this.laf.backgroundImageSrc)
        this.images = new Map();
        this.imageDrawBuffer = [];
        for (let a = 0; a < imagesToLoad.length; a++) {
            imgSrc = imagesToLoad[a];
            if (imgSrc != null) {
                let img = new Image();
                img.src = imgSrc;
                let imageUse;
                switch (a) {
                    case 0:
                        imageUse = UrCellType.SAFE_ZONE;
                        break;
                    case 1:
                        imageUse = UrCellType.FINAL_FINISH;
                        break;
                    case 2:
                        imageUse = UrLAFUse.BACKGROUND;
                        break;
                }
                this.images.set(imageUse, img);
                img.loaded = false;
                img.onload = () => {
                    img.loaded = true;
                    let allLoaded = true;
                    this.images.forEach(img => {
                        if (!img.loaded) {
                            allLoaded = false;
                        }
                    });
                    if (allLoaded) {
                        this.afterLoading();
                    }
                }
            }
        }
    }

    afterLoading() {
        this.imageDrawBuffer.forEach(imageDrawCommand => {
            this.ctx.drawImage(
                imageDrawCommand.img,
                imageDrawCommand.cellPosition.x,
                imageDrawCommand.cellPosition.y,
                this.lm.cellSize, this.lm.cellSize);
        });
        this.imageDrawBuffer.length = 0;
        this.laf.afterLoading();
        this.drawBoard();
        this.drawUI();
    }

    setCells(cells) {
        this.cells = cells;
    }

    setUnusedCounters(unusedCounters) {
        this.unusedCounters = unusedCounters;
        this.nUnusedCounters = this.unusedCounters.length;
    }

    drawUI() {
        this.uiManager.draw(this);
    }

    updateCounterSize() {
        this.counterSize = this.lm.cellSize * 0.25;
    }

    updateSize() {
        this.updateCounterSize();
        this.laf.updateFonts();
        this.drawBoard();
        this.drawUI();
    }

    drawBoard() {
        this.lm.updateSize();
        this.laf.drawBackground(this.ctx);
        this.cells.forEach(cell => this.drawCell(cell));
        let counter;
        let nextColumns = new Map();
        let columnNumber;
        for (let a = 0; a < this.nUnusedCounters; a++) {
            counter = this.unusedCounters[a];
            columnNumber = nextColumns.get(counter.player);
            if (columnNumber == null) {
                columnNumber = 0;
            } else {
                columnNumber++;
            }
            nextColumns.set(counter.player, columnNumber);
            const counterDrawProperties = this.lm.cellPosition(
                columnNumber,
                counter.player.unusedCountersYCellNumber);
            counterDrawProperties.counter = counter;
            counterDrawProperties.x += this.lm.halfCellSize;
            counterDrawProperties.y += this.lm.halfCellSize;
            this.drawCounter(counterDrawProperties);
        }
    }

    drawCell(cell) {
        let cellPosition = this.lm.cellPosition(cell.xCellNumber, cell.yCellNumber);
        this.ctx.beginPath();
        this.laf.drawCell(cellPosition, cell);
        if (cell.type != null) {
            let img = this.images.get(cell.type);
            if (img != null) {
                if (img.loaded != null && img.loaded) {
                    this.ctx.drawImage(
                        img, cellPosition.x, cellPosition.y,
                        this.lm.cellSize, this.lm.cellSize);
                } else {
                    this.imageDrawBuffer.push(
                        {
                            img: img,
                            cellPosition: cellPosition
                        });
                }
            }
        }
        if (cell.counter != null && !cell.counter.moving) {
            this.drawCounter(
                {
                    x: cellPosition.x + this.lm.halfCellSize,
                    y: cellPosition.y + this.lm.halfCellSize,
                    counter: cell.counter
                });
        }
    }

    drawCounter(counterDrawProperties) {
        this.laf.counterSize = this.counterSize;
        this.laf.drawCounter(counterDrawProperties);
    }

    visualiseCounterMovement(currentCell, nextCell, onAnimationFinish) {
        this.onAnimationFinish = onAnimationFinish;
        const counter = currentCell.counter;
        const positions = [];
        positions.push(this.lm.cellMiddlePosition(currentCell.xCellNumber, currentCell.yCellNumber));
        if (currentCell.pathStep < this.model.nHomeCells - 1
            && nextCell.pathStep >= this.model.nHomeCells) {
            const endOfHomeRow = this.model.getCellByPlayerAndPathOrder(counter.player, this.model.nHomeCells);
            positions.push(this.lm.cellMiddlePosition(endOfHomeRow.xCellNumber, endOfHomeRow.yCellNumber));
        }
        if (currentCell.pathStep < this.model.nHomeCells
            && nextCell.pathStep >= this.model.nHomeCells) {
            const startOfWarCells = this.model.getCellByPlayerAndPathOrder(counter.player, this.model.nHomeCells + 1);
            positions.push(this.lm.cellMiddlePosition(startOfWarCells.xCellNumber, startOfWarCells.yCellNumber));
        }
        const victoryTurnStep = this.model.pathLength - this.model.nFinishingCells - 1;
        if (currentCell.pathStep < victoryTurnStep
            && nextCell.pathStep >= victoryTurnStep + 1) {
            const endOfWarRow = this.model.getCellByPlayerAndPathOrder(counter.player, victoryTurnStep + 1);
            positions.push(this.lm.cellMiddlePosition(endOfWarRow.xCellNumber, endOfWarRow.yCellNumber));
        }
        if (currentCell.pathStep < victoryTurnStep + 1
            && nextCell.pathStep >= victoryTurnStep + 2) {
            const startOfVictoryRow = this.model.getCellByPlayerAndPathOrder(counter.player, victoryTurnStep + 2);
            positions.push(this.lm.cellMiddlePosition(startOfVictoryRow.xCellNumber, startOfVictoryRow.yCellNumber));
        }
        positions.push(this.lm.cellMiddlePosition(nextCell.xCellNumber, nextCell.yCellNumber));
        this.animation = new UrAnimation({
            counter,
            positions,
            view: this
        });
        this.animation.play();
    }

    animationFinished() {
        this.animation = null;
        this.onAnimationFinish();
    }

    setNumberOfDice(nDice) {
        this.nDice = nDice;
    }

    displayDiceRoll(diceNumber, diceRoll) {
        this.laf.diceRollFont();
        if (diceNumber > 0) {
            this.ctx.fillText("+", this.lm.diceRollLeft + this.lm.diceRollWidth * (diceNumber - 0.5), this.lm.diceRollTop);
        }
        this.ctx.fillText(diceRoll, this.lm.diceRollLeft + this.lm.diceRollWidth * diceNumber, this.lm.diceRollTop);
    }

    displayDieResult(result) {
        this.laf.dieResultFont();
        this.ctx.fillText("=", this.lm.diceRollLeft + this.lm.diceRollWidth * (this.nDice - 0.5), this.lm.diceRollTop);
        this.ctx.fillText(result, this.lm.diceRollLeft + this.lm.diceRollWidth * this.nDice, this.lm.diceRollTop);
    }

    displayMessage1(messageText) {
        this.drawMessage(messageText, this.lm.height * 0.05);
    }

    displayMessage2(messageText) {
        if (this.lm.width < this.lm.height) {
            this.drawMessage(messageText, this.lm.height * 0.09);
        } else {
            this.drawMessage(messageText, this.lm.height * 0.1);
        }
    }

    drawMessage(messageText, y) {
        if (this.lm.width < this.lm.height) {
            y += this.lm.height * 0.75;
        }
        this.laf.messageFont();
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 2;
        this.ctx.strokeText(messageText,
            this.lm.boardLeft, y);
        this.ctx.fillStyle = "white";
        this.ctx.fillText(messageText,
            this.lm.boardLeft, y);
    }
}