
/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrRandom {
    constructor(seed) {
        this.nCalls = 0;
        this.seed(seed);
    }

    // Takes any integer
    seed(seed) {
        seed = Math.floor(seed);
        this.parts = [seed % 567457,seed % 76885,seed % 12356,seed % 907,seed % 5,seed % 4557,seed % 7656,seed % 2134,seed % 35467];
    }

    // Returns number between 0 (inclusive) and 1.0 (exclusive),
    // just like Math.random().
    random() {
        const str = "" + this.parts;
        const hash = sha256(str);
        const numberFromHash = parseInt(hash, 16);
        this.seed(numberFromHash + (this.nCalls++));
        let output = Math.abs((numberFromHash % 100000000) / 100000000);
        if(output >= 1.0) {
            output -= 1.0;
        }
        output = Math.abs(output);
        return output;

        // return Math.random();
    }
}
