"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrCell {
    constructor(props) {
        const {xCellNumber, yCellNumber, owner, pathOrder, safe} = props;
        this.xCellNumber = xCellNumber;
        this.yCellNumber = yCellNumber;
        this.counter = null;
        this.owner = owner;
        this.pathOrder = pathOrder;
        this.safe = safe;
        if (this.xCellNumber == null) {
            console.error("xCellNumber is null");
        }
        if (this.yCellNumber == null) {
            console.error("xCellNumber is null");
        }
        if (this.pathOrder == null) {
            console.error("pathOrder is null");
        }
    }
}
