"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
const UrInputToWaitFor = Object.freeze(
    {
        ROLL_DICE: Symbol("roll dice"),
        MOVE_CHOICE: Symbol("move choice"),
        NEXT_PLAYER_ON_CLICK: Symbol("next player on click"),
        NEXT_SLIDE: Symbol("next slide")
    });

const UrCellType = Object.freeze(
    {
        SAFE_ZONE: Symbol("safe zone"),
        FINAL_FINISH: Symbol("final finish")
    });

const UrLAFUse = Object.freeze(
    {
        BACKGROUND: Symbol("background")
    });

const UrPlayMode = Object.freeze(
    {
        HUMAN_VS_HUMAN: Symbol("Human vs. Human"),
        PLAYER_VS_COMPUTER: Symbol("Human vs. Computer"),
        HOW_TO_PLAY: Symbol("How To Play")
    }
)

const UrAbout = Object.freeze({
    ABOUT: Symbol("About"),
    BACK: Symbol("Back")
})

const UrGameOverAction = Object.freeze(
    {
        REFRESH: Symbol("Refresh")
    }
)

class UrModel {
    constructor(app) {
        const seed = now();
        // const seed = 1603037775248;
        this.tutorialSlide = -1;
        this.gameOver = false;
        this.winner = null;
        this.ai = null;
        this.random = new UrRandom(seed);
        console.log("Seed for this game is " + seed);
        this.randomRequest = 1;
        this.app = app;
        this.view = app.view;
        this.allCells = [];
        this.player1 = new UrPlayer({
            name: "Player 1",
            homeYCellNumber: 0,
            model: this,
            fillStyle: "#B3897D",
            selectedStrokeStyle: "white",
            deselectedStrokeStyle: "black"
        });
        this.player2 = new UrPlayer({
            name: "Player 2",
            homeYCellNumber: 2,
            model: this,
            fillStyle: "#1F1F25",
            selectedStrokeStyle: "red",
            deselectedStrokeStyle: "gray"
        });
        this.players = [];
        this.players.push(this.player1);
        this.players.push(this.player2);
        this.nPlayers = this.players.length;
        this.currentPlayer = this.player1;
        this.currentPlayerIndex = 0;
        this.homeStartColumn = 3;
        this.homeEndColumn = 0;
        this.warStartColumn = 0;
        this.warEndColumn = 7;
        this.warYCellNumber = 1;
        this.pathLength = 14;
        this.finishingStart = 7;
        this.finishingEnd = 6;
        this.nHomeCells = Math.abs(this.homeStartColumn - this.homeEndColumn) + 1;
        this.warCells = [];
        this.nWarCells = Math.abs(this.warEndColumn - this.warStartColumn) + 1;
        this.nFinishingCells = Math.abs(this.finishingEnd - this.finishingStart) + 1;
        this.nDice = 2;
        this.nDicePoints = 4;
        this.nCountersPerPlayer = 7;
        this.view.setNumberOfDice(this.nDice);
        this.unusedCounters = [];
    }

    startGame(mode) {
        switch (mode) {
            case UrPlayMode.HOW_TO_PLAY:
                this.player1.human = true;
                this.player2.human = false;
                this.tutorialSlide = 0;
                break;
            case UrPlayMode.PLAYER_VS_COMPUTER:
                this.player1.human = true;
                this.player2.human = false;
                break;
            case UrPlayMode.HUMAN_VS_HUMAN:
                this.player1.human = true;
                this.player2.human = true;
                break;
            default:
                console.warn("Unknown play mode");
        }
    }

    getOtherPlayer() {
        if (this.currentPlayer === this.player1) {
            return this.player2;
        } else {
            return this.player1;
        }
    }

    generateCell(props) {
        let player;
        const {xCellNumber, yCellNumber, owner, pathOrder} = props;
        const cell = new UrCell({
            xCellNumber, yCellNumber, owner, pathOrder, safe: pathOrder === 8
        });
        this.allCells.push(cell);
        if (owner == null) {
            for (let playerIndex = 0; playerIndex < this.nPlayers; playerIndex++) {
                player = this.players[playerIndex];
                player.fullPath.push(cell);
            }
            cell.pathStep = player.fullPath.length - 1;
        } else {
            cell.pathStep = owner.fullPath.length;
            owner.fullPath.push(cell);
        }
        return cell;
    }

    getCellByPlayerAndPathOrder(owner, pathOrder) {
        let result = null;
        this.allCells.forEach(cell => {
            if ((cell.owner == null || cell.owner === owner) && cell.pathOrder === pathOrder) {
                result = cell;
            }
        });
        return result;
    }

    generateHomeCells(player) {
        let pathOrder = 1;
        for (let xCellNumber = this.homeStartColumn; xCellNumber >= this.homeEndColumn; xCellNumber--) {
            player.homeCells.push(this.generateCell({
                xCellNumber,
                yCellNumber: player.homeYCellNumber,
                owner: player,
                pathOrder: pathOrder
            }));
            pathOrder++;
        }
    }

    generateWarCells() {
        let cell;
        let pathOrder = this.nHomeCells + 1;
        for (let xCellNumber = this.warStartColumn; xCellNumber <= this.warEndColumn; xCellNumber++) {
            cell = this.generateCell({
                xCellNumber,
                yCellNumber: this.warYCellNumber,
                pathOrder: pathOrder
            });
            pathOrder++;
            this.warCells.push(cell);
            if (xCellNumber === 3) {
                cell.type = UrCellType.SAFE_ZONE;
            }
        }
    }

    generateFinishingCells(player) {
        let cell;
        let pathOrder = this.pathLength - this.nFinishingCells + 1;
        for (let xCellNumber = this.finishingStart; xCellNumber >= this.finishingEnd; xCellNumber--) {
            cell = this.generateCell({
                xCellNumber: xCellNumber,
                yCellNumber: player.homeYCellNumber,
                owner: player,
                pathOrder: pathOrder
            });
            pathOrder++;
            player.finishingCells.push(cell);
            if (xCellNumber === this.finishingEnd) {
                cell.type = UrCellType.FINAL_FINISH;
            }
        }
    }

    updateUnusedCounters() {
        this.unusedCounters = [];
        let player;
        for (let playerIndex = 0; playerIndex < this.nPlayers; playerIndex++) {
            player = this.players[playerIndex];
            this.unusedCounters = this.unusedCounters.concat(player.unusedCounters);
        }
        this.view.setUnusedCounters(this.unusedCounters);
    }

    begin() {
        let player;
        for (let playerIndex = 0; playerIndex < this.nPlayers; playerIndex++) {
            player = this.players[playerIndex];
            for (let counterNumber = 0; counterNumber < this.nCountersPerPlayer; counterNumber++) {
                player.makeUnusedCounter();
            }
            this.generateHomeCells(player);
        }
        this.updateUnusedCounters();
        this.generateWarCells();
        for (let playerIndex = 0; playerIndex < this.nPlayers; playerIndex++) {
            player = this.players[playerIndex];
            this.generateFinishingCells(player);
        }
        this.view.setCells(this.allCells);
        this.view.drawBoard();
        this.view.drawUI();
    }

    waitForRollDiceCommand() {
        if (this.currentPlayer.human) {
            this.waitingForInput = UrInputToWaitFor.ROLL_DICE;
            if (this.tutorialSlide < 0) {
                this.view.displayMessage1(this.currentPlayer.name + ": Click to roll dice");
            }
        } else {
            this.ai.makeMove();
        }
    }

    beginTutorial() {
        showCursor("pointer");
        this.waitingForInput = UrInputToWaitFor.NEXT_SLIDE;
        this.view.displayMessage1("Welcome to the Game of Ur!");
        this.view.displayMessage2("Click to continue...");
    }


    spaceOnHomeRowAvailable(dieResult) {
        if (dieResult == null) {
            dieResult = this.dieResult;
        }
        const cell = this.getCellByPlayerAndPathOrder(this.currentPlayer, this.dieResult);
        if (cell == null) {
            console.warn("Cell is null");
            return false;
        } else {
            return cell.counter == null;
        }
    }

    clickDoesSomething(cellNumbers) {
        if (this.waitingForInput === UrInputToWaitFor.NEXT_SLIDE) {
            return true;
        } else if (this.waitingForInput === UrInputToWaitFor.NEXT_PLAYER_ON_CLICK
            || this.waitingForInput === UrInputToWaitFor.ROLL_DICE) {
            return true;
        } else if (this.waitingForInput === UrInputToWaitFor.MOVE_CHOICE) {
            if (cellNumbers.yCellNumber === this.currentPlayer.unusedCountersYCellNumber) {
                //Unused counters / home row
                if (this.dieResult === 0) {
                    return false;
                } else if (this.dieResult <= this.nHomeCells && this.spaceOnHomeRowAvailable()) {
                    //Can move onto home row
                    return true;
                } else {
                    if (this.currentPlayer.getNumberOfCountersOnBoard() > 0) {
                        //The user needs to click on one of the counters that are
                        //already on the board
                        return false;
                    } else {
                        //The user can't do anything, so go to the next turn
                        return true;
                    }
                    // If dice on board > 0: allow player to move
                    // Otherwise go to next turn
                }
            } else {
                let counter = this.currentPlayer.getCounter(cellNumbers);
                if (counter != null && counter.cell != null) {
                    const piecesThatCanMove = this.currentPlayer.getPiecesInPlayThatCanMove(this.dieResult);
                    if (piecesThatCanMove.indexOf(counter) < 0) {
                        //No space to move!
                        return false;
                    }
                    const spacesToMove = this.dieResult;
                    const endCellNumber = counter.nextSpaceNumber(spacesToMove);
                    if (endCellNumber > this.pathLength) {
                        //End cell is too far!
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
    }

    cellPicked(cellNumbers) {
        if (this.gameOver) {
            return;
        }
        if (this.waitingForInput === UrInputToWaitFor.NEXT_SLIDE) {
            this.nextSlide();
        } else if (this.waitingForInput === UrInputToWaitFor.NEXT_PLAYER_ON_CLICK) {
            this.view.drawBoard();
            this.nextPlayer();
            this.waitForRollDiceCommand();
        } else if (this.waitingForInput === UrInputToWaitFor.ROLL_DICE) {
            if (this.rollDie()) {
                this.waitingForInput = UrInputToWaitFor.MOVE_CHOICE;
                this.view.displayMessage2(this.currentPlayer.name + ": Select a piece to move");
            } else {
                this.waitingForInput = UrInputToWaitFor.NEXT_PLAYER_ON_CLICK;
                this.view.displayMessage2(this.currentPlayer.name + ": No pieces can move");
            }
        } else if (this.waitingForInput === UrInputToWaitFor.MOVE_CHOICE) {
            if (cellNumbers.yCellNumber === this.currentPlayer.unusedCountersYCellNumber) {
                //Unused counters / home row
                if (this.dieResult === 0) {
                    // && this.currentPlayer.getNumberOfCountersOnBoard() === 0
                    this.view.drawBoard();
                    this.view.displayMessage1(this.currentPlayer.name + ": Unable to move");
                    this.view.displayMessage2(this.currentPlayer.name + ": Click to end turn");
                } else if (this.dieResult <= this.nHomeCells && this.spaceOnHomeRowAvailable()) {
                    this.currentPlayer.placeUnusedCounterOnBoard(this.dieResult);
                    this.updateUnusedCounters();
                    this.view.drawBoard();
                    this.nextPlayer();
                    if (this.currentPlayer !== this.players[0] || this.tutorialSlide < 0) {
                        this.waitForRollDiceCommand();
                    } else {
                        this.waitingForInput = UrInputToWaitFor.NEXT_SLIDE;
                    }
                } else {
                    this.view.drawBoard();
                    this.view.displayMessage1(this.currentPlayer.name + ": Unable to fit on home row");
                    if (this.currentPlayer.getNumberOfCountersOnBoard() > 0) {
                        this.view.displayMessage2(this.currentPlayer.name + ": Select a piece to move");
                    } else {
                        this.view.displayMessage2(this.currentPlayer.name + ": Click to end turn");
                        this.waitingForInput = UrInputToWaitFor.NEXT_PLAYER_ON_CLICK;
                    }
                    // If dice on board > 0: allow player to move
                    // Otherwise go to next turn
                }
            } else {
                let counter = this.currentPlayer.getCounter(cellNumbers);
                if (counter != null && counter.cell != null) {
                    const piecesThatCanMove = this.currentPlayer.getPiecesInPlayThatCanMove(this.dieResult);
                    if (piecesThatCanMove.indexOf(counter) < 0) {
                        this.view.drawBoard();
                        this.view.displayMessage1(this.currentPlayer.name + ": No space to move!");
                        return;
                    }
                    const spacesToMove = this.dieResult;
                    const endCellNumber = counter.nextSpaceNumber(spacesToMove);
                    if (endCellNumber > this.pathLength) {
                        console.log("End cell number = " + endCellNumber + ", too far!");
                        this.waitingForInput = UrInputToWaitFor.NEXT_PLAYER_ON_CLICK;
                        this.view.drawBoard();
                        this.view.displayMessage2(this.currentPlayer.name + ": Too far!");
                        return;
                    }
                    const spaceNumber = counter.spacesAhead(spacesToMove);
                    doingSomething = true;
                    this.view.visualiseCounterMovement(
                        counter.cell,
                        spaceNumber,
                        () => {
                            doingSomething = false;
                            counter.moveForward(spacesToMove);
                            this.view.drawBoard();
                            if (endCellNumber === this.pathLength - 1) {
                                this.currentPlayer.counterScored(counter);
                                if (this.currentPlayer.unusedCounters.length === 0
                                    && this.currentPlayer.getNumberOfCountersOnBoard() === 0) {
                                    this.view.displayMessage1(this.currentPlayer.name + " has won!");
                                    this.gameOver = true;
                                    this.winner = this.currentPlayer;
                                    this.app.uiManager.dialogs[1].name = this.currentPlayer.name + " wins!";
                                    this.app.uiManager.showDialog(1);
                                    this.view.drawUI();
                                } else {
                                    this.view.displayMessage1(this.currentPlayer.name + " has scored!");
                                    if (this.currentPlayer.human) {
                                        this.waitingForInput = UrInputToWaitFor.NEXT_PLAYER_ON_CLICK;
                                    } else {
                                        doLater(() => {
                                            this.view.drawBoard();
                                            this.nextPlayer();
                                            this.waitForRollDiceCommand();
                                        }, 1000);
                                    }
                                }
                            } else {
                                this.nextPlayer();
                                this.waitForRollDiceCommand();
                            }
                            this.view.drawUI();
                        });
                }
            }
        }
        this.view.drawUI();
    }

    nextSlide() {
        this.tutorialSlide++;
        this.ai.easyMode = true;
        this.view.drawBoard();
        switch (this.tutorialSlide) {
            case 1:
                this.view.displayMessage1("The Game of Ur is an ancient race game.");
                this.view.displayMessage2("The objective is to get all your pieces to the end.");
                break;
            case 2:
                this.view.displayMessage1("At the start of each turn a pair of tetrahedral dice");
                this.view.displayMessage2("are rolled. The values on the dice are 0, 1, 2 and 3.");
                break;
            case 3:
                this.view.displayMessage1("Adding the pair of numbers together gives you the");
                this.view.displayMessage2("number of spaces over which you can move a piece.");
                break;
            case 4:
                this.view.displayMessage1("The pieces are circles that are");
                this.view.displayMessage2("currently placed at the side of the board.");
                break;
            case 5:
                this.view.displayMessage1("To start moving a piece around the board you must");
                this.view.displayMessage2("first move it onto your home row.");
                break;
            case 6:
                if (this.view.lm.width < this.view.lm.height) {
                    this.view.displayMessage1("Your home row is the line of cells near the left");
                } else {
                    this.view.displayMessage1("Your home row is the line of cells near the top");
                }
                this.view.displayMessage2("of the board labelled 1,2,3 and 4.");
                break;
            case 7:
                this.view.displayMessage1("The die result must be between 1 and 4.");
                this.view.displayMessage2("Click to roll the dice...");
                break;
            case 8:
                this.random.seed(0);
                this.rollDie();
                this.view.displayMessage2("You can move to the home row! Click on a piece.");
                this.waitingForInput = UrInputToWaitFor.MOVE_CHOICE;
                break;
            case 9:
                this.view.displayMessage1("Your opponent was also able to move a piece to their");
                this.view.displayMessage2("home row. Your piece is currently safe.");
                break;
            case 10:
                this.view.displayMessage1("Now that you have a piece on your home row you");
                this.view.displayMessage2("can move it to the war path.");
                break;
            case 11:
                this.view.displayMessage1("On the war path a piece can send back an enemy");
                this.view.displayMessage2(" piece by landing on top of it.");
                break;
            case 12:
                this.view.displayMessage1("The green star on cell 8 is safe.");
                this.view.displayMessage2("If you land a piece there it can't be sent back.");
                break;
            case 13:
                this.view.displayMessage1("Click to roll the dice again...");
                break;
            case 14:
                this.random.seed(0);
                this.rollDie();
                this.view.displayMessage2("Success! Select a piece to move.");
                this.waitingForInput = UrInputToWaitFor.MOVE_CHOICE;
                break;
            case 15:
                this.view.displayMessage1("Now you're on the war path. Keep moving all");
                this.view.displayMessage2("your pieces until they get to the gold star.");
                this.waitingForInput = UrInputToWaitFor.NEXT_SLIDE;
                break;
            case 16:
                this.view.displayMessage1("If you roll a number too low or too");
                this.view.displayMessage2("high you won't be able to move.");
                break;
            case 17:
                this.view.displayMessage1("Once you get a piece to cell 13 it's safe.");
                this.view.displayMessage2("Good luck!");
                break;
            case 18:
                this.waitingForInput = UrInputToWaitFor.MOVE_CHOICE;
                this.tutorialSlide = -2;
                break;
        }
    }

    nextPlayer() {
        if (!this.gameOver) {
            this.currentPlayer.turnOver();
            this.currentPlayerIndex = this.currentPlayerIndex + 1;
            if (this.currentPlayerIndex === this.nPlayers) {
                this.currentPlayerIndex = 0;
            }
            this.currentPlayer = this.players[this.currentPlayerIndex];
            this.view.drawBoard();
            this.view.drawUI();
            if (this.currentPlayer === this.players[0] && this.tutorialSlide >= 0) {
                this.nextSlide();
            }
        }
    }

    singleDiceRoll() {
        // return Math.floor((this.randomRequest += 2) % this.nDicePoints);
        // return Math.floor(Math.random() * this.nDicePoints);
        // return 1;
        return Math.floor(this.random.random() * this.nDicePoints);
    }

    rollDie() {
        if (!this.gameOver) {
            this.dieResult = 0;
            let diceRoll;
            const diceRolls = new Array(this.nDice);
            for (let diceNumber = 0; diceNumber < this.nDice; diceNumber++) {
                diceRoll = this.singleDiceRoll();
                this.dieResult += diceRoll;
                diceRolls[diceNumber] = diceRoll;
            }
            const piecesThatCanMove = this.currentPlayer.markPiecesThatCanMove(this.dieResult);
            this.piecesThatCanMove = piecesThatCanMove;
            this.view.drawBoard();
            for (let diceNumber = 0; diceNumber < this.nDice; diceNumber++) {
                this.view.displayDiceRoll(diceNumber, diceRolls[diceNumber]);
            }
            this.view.displayDieResult(this.dieResult);
            this.view.drawUI();
            return piecesThatCanMove.length > 0;
        } else {
            return 0;
        }
    }
}