"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrController {
    constructor(application) {
        this.model = application.model;
        this.lm = application.locationManager;
        this.uiManager = application.uiManager;
        this.canvasID = application.canvasID;
        document.onmousemove = event => this.mouseMove(event);
        document.onmouseup = event => this.mouseUp(event);
    }

    begin() {
        this.model.begin();
    }

    mouseUp(event) {
        if (!doingSomething) {
            if (this.model.gameOver && !this.uiManager.dialogShowing) {
                this.uiManager.showDialog(1);
            } else if (this.uiManager.dialogShowing) {
                const rect = document.body.getBoundingClientRect();
                const point = {x: event.x - rect.x, y: event.y - rect.y};
                this.uiManager.uiClick(point);
            } else if (this.model.currentPlayer.human) {
                this.model.cellPicked(this.lm.cellNumbers(event.x, event.y));
            }
        } else {
            console.warn("Can't mouseUp - in the middle of doing something!")
        }
    }

    rotated(point) {
        return this.lm.rotated(point);
    }

    mouseMove(event) {
        let clickable = false;
        if (this.uiManager.dialogShowing) {
            const rect = document.body.getBoundingClientRect();
            const point = {x: event.x - rect.x, y: event.y - rect.y};
            clickable = clickable || this.uiManager.uiMouseMove(point);
        } else {
            if (this.model.currentPlayer.human) {
                clickable = clickable || this.model.clickDoesSomething(this.lm.cellNumbers(event.x, event.y));
            }
        }
        if (doingSomething) {
            clickable = false;
        }
        if (clickable) {
            showCursor("pointer");
        } else {
            showCursor("auto");
        }
    }
}