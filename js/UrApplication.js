/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
let canvasID = null;

class UrApplication {
    begin() {
        this.canvasID = "canvas";
        canvasID = this.canvasID;
        this.locationManager = new UrCanvasLocationManager(this.canvasID);
        this.lookAndFeel = new UrLookAndFeel(this.locationManager);
        this.view = new UrView({
            lm: this.locationManager,
            laf: this.lookAndFeel
        });
        this.lookAndFeel.setView(this.view);
        this.locationManager.view = this.view;
        this.model = new UrModel(this);
        this.view.model = this.model;
        this.ai = new UrAI(this.model);
        this.model.ai = this.ai;
        this.uiManager = new UrUIManager(this);
        this.view.uiManager = this.uiManager;
        this.controller = new UrController(this);
        this.controller.begin();
    }
}