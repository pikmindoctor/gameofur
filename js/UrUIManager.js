/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrUIManager {
    titleHeight = 50;
    dialogShowing = true;
    dialogs = [
        {
            "name": "Game of Ur",
            "width": 256,
            "buttons": [
                {
                    text: "How To Play",
                    symbol: UrPlayMode.HOW_TO_PLAY
                },
                {
                    text: "Human vs. Computer",
                    symbol: UrPlayMode.PLAYER_VS_COMPUTER
                },
                {
                    text: "Human vs. Human",
                    symbol: UrPlayMode.HUMAN_VS_HUMAN
                },
                {
                    text: "About",
                    symbol: UrAbout.ABOUT
                }
            ]
        },
        {
            "name": "Game Over",
            "width": 256,
            "buttons": [
                {
                    text: "Play Again",
                    symbol: UrGameOverAction.REFRESH
                }]
        },
        {
            "name": "About",
            "width": 350,
            "message":
                "Based on a concept developed during\n" +
                "the early third millenium BC.\n" +
                "Implemented by Lawrence Watkiss-Veal.\n" +
                "lwatkissveal@gmail.com",
            "messageHeight": 120,
            "buttons": [
                {
                    text: "OK",
                    symbol: UrAbout.BACK
                }]
        }
    ];
    currentDialog = this.dialogs[0];

    showDialog(dialogIndex) {
        this.currentDialog = this.dialogs[dialogIndex];
        this.dialogShowing = true;
    }

    constructor(props) {
        this.model = props.model;
        this.view = props.view;
        this.laf = props.lookAndFeel;
    }

    getButtonSize() {
        return {width: width - 32, height: 25};
    }

    uiMouseMove(event) {
        let clickable = false;
        if (this.currentDialog != null && this.dialogShowing) {
            const buttons = this.currentDialog.buttons;
            const n = buttons.length;
            let button;
            let rect;
            for (let row = 0; row < n; row++) {
                button = buttons[row];
                rect = this.getButtonDrawProperties(button, row).rect;
                button.highlighted = rectContains(event, rect);
                clickable = clickable || button.highlighted;
            }
            this.view.drawUI();
        }
        return clickable;
    }

    uiClick(event) {
        if (this.currentDialog != null && this.dialogShowing) {
            const buttons = this.currentDialog.buttons;
            const n = buttons.length;
            let button;
            for (let row = 0; row < n; row++) {
                button = buttons[row];
                if (rectContains(event, this.getButtonDrawProperties(button, row).rect)) {
                    if (button.symbol === UrGameOverAction.REFRESH) {
                        document.location.reload();
                    } else if (button.symbol === UrAbout.BACK) {
                        this.view.drawBoard();
                        this.showDialog(0);
                        this.calculate();
                        this.draw();
                    } else {
                        this.dialogShowing = false;
                        if (button.symbol === UrAbout.ABOUT) {
                            this.view.drawBoard();
                            this.showDialog(2);
                            this.calculate();
                            this.draw();
                        } else {
                            this.model.startGame(button.symbol);
                            this.view.drawBoard();
                            if (this.model.tutorialSlide < 0) {
                                this.model.waitForRollDiceCommand();
                            } else {
                                this.model.beginTutorial();
                            }
                        }
                    }
                }
            }
        }
    }

    calculate() {
        this.middleX = this.canvas.width / 2;
        this.middleY = this.canvas.height / 2;
        this.buttonSize = {width: this.width - 32, height: 30};
        this.height = this.buttonSize.height * (this.currentDialog.buttons.length + 1) + this.titleHeight;
        if (this.currentDialog.message != null) {
            this.height += this.currentDialog.messageHeight;
        }
        this.width = this.currentDialog.width;
        this.left = this.middleX - this.width / 2;
        this.right = this.middleX + this.width / 2;
        this.top = this.middleY - this.height / 2;
        this.bottom = this.middleY + this.height / 2;
        this.buttonRowHeight = this.buttonSize.height + 4;
    }

    draw() {
        this.ctx = this.view.ctx;
        this.canvas = this.view.canvas;
        this.calculate();
        if (this.dialogShowing) {
            this.view.drawBoard();
            this.drawDialog();
        }
    }

    drawDialog() {
        this.ctx.resetTransform();
        this.ctx.fillStyle = "black";
        const shadowDistance = 3;
        this.ctx.fillRect(
            this.left + shadowDistance, this.top + shadowDistance,
            this.width + shadowDistance, this.height + shadowDistance);
        this.ctx.fillStyle = "white";
        this.ctx.fillRect(this.left, this.top, this.width, this.height);
        this.ctx.strokeStyle = "black";
        this.ctx.strokeRect(this.left, this.top, this.width, this.height);
        this.ctx.fillStyle = "black";
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "middle";
        this.ctx.font = this.laf.uiDialogTitleFont;
        this.ctx.fillText(this.currentDialog.name, this.middleX, this.top + 26);
        this.ctx.beginPath();
        this.ctx.moveTo(this.left, this.top + this.titleHeight);
        this.ctx.lineTo(this.right, this.top + this.titleHeight);
        this.ctx.stroke();
        this.ctx.font = this.laf.uiDialogContentFont;
        let rowNumber = 0;
        let buttonDrawProperties;
        if (this.currentDialog.message != null) {
            const lines = this.currentDialog.message.split("\n");
            for (let ln = 0; ln < lines.length; ln++) {
                this.ctx.fillText(lines[ln], this.middleX, this.top + this.titleHeight + 26 * (ln + 1));
            }
        }
        this.currentDialog.buttons.forEach(button => {
            buttonDrawProperties = this.getButtonDrawProperties(button, rowNumber++);
            this.drawButton({
                text: button.text,
                position: buttonDrawProperties.position,
                size: buttonDrawProperties.buttonSize,
                rect: buttonDrawProperties.rect,
                highlighted: button.highlighted
            });
        });
    }

    getButtonDrawProperties(button, rowNumber) {
        const position = {
            x: this.middleX,
            y: this.top + this.titleHeight + this.buttonRowHeight * (rowNumber + 0.5) + 8
        };
        if (this.currentDialog.message != null) {
            position.y += this.currentDialog.messageHeight;
        }
        return {
            position,
            size: this.buttonSize,
            rect: {
                x: position.x - this.buttonSize.width / 2,
                y: position.y - this.buttonSize.height * 0.5,
                width: this.buttonSize.width,
                height: this.buttonSize.height
            }
        };
    }

    drawButton(properties) {
        const {text, position, rect} = properties;
        this.ctx.font = this.laf.uiButtonFont;
        if (properties.highlighted) {
            this.ctx.fillStyle = "gray";
            this.ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
            this.ctx.fillStyle = "white"
            this.ctx.fillText(text, position.x, position.y);
        } else {
            this.ctx.fillStyle = "black";
            this.ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
            this.ctx.fillStyle = "white"
            this.ctx.fillText(text, position.x, position.y);
        }
    }
}